package bean;

import bean.javabean;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.beans.SimpleBeanInfo;

public class javabeanBeanInfo extends SimpleBeanInfo {
    public PropertyDescriptor[] getPropertyDescriptors() {
        try {
            PropertyDescriptor f1c = new PropertyDescriptor("title", javabean.class);
            PropertyDescriptor f2c = new PropertyDescriptor("kolor", javabean.class);
            PropertyDescriptor[] list = {f1c, f2c};
            return list;
        } catch (IntrospectionException e) {
            e.printStackTrace();
            return null;
        }

    }
}
