package bean;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.*;
import java.io.Serializable;

import static java.awt.Color.black;
import static java.awt.Color.white;

public class javabean  extends JPanel implements Serializable {
    private JTextField DeafaultField;
    private JTextField ConvertField;
    private JTextField ConvertField2;
    private JRadioButton pxRadioButton;
    private double defaultfontSize;
    private double toConvert;
    private double Convert;
    private JRadioButton remRadioButton;
    private JRadioButton ptRadioButton;
    private JRadioButton pxRadioButton1;
    private JRadioButton remRadioButton1;
    private JRadioButton ptRadioButton1;
    private JPanel Pane;
    private JRadioButton radioButton1;
    private JRadioButton radioButton2;
    private JButton changeColorButton;
    private Color kolor;
    private String title;
    private PropertyChangeSupport changes;
    private VetoableChangeSupport vetoes;
    private int numbers;
    String wybor;
    public javabean(){
        remRadioButton.addActionListener(listener);
        remRadioButton1.addActionListener(listener);
        ptRadioButton.addActionListener(listener);
        ptRadioButton1.addActionListener(listener);
        pxRadioButton1.addActionListener(listener);
        pxRadioButton.addActionListener(listener);
        remRadioButton.addActionListener(listener);
        radioButton1.addActionListener(listener);
        radioButton2.addActionListener(listener);
        changeColorButton.addActionListener(color);

        kolor=white;
        changes = new PropertyChangeSupport
                (this);
        vetoes =
                new VetoableChangeSupport (this);
        DeafaultField.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent arg0)
            {
               defaultfontSize = Double.parseDouble(DeafaultField.getText());
            }
            public void insertUpdate(DocumentEvent arg0)
            {
                defaultfontSize = Double.parseDouble(DeafaultField.getText());
            }

            public void removeUpdate(DocumentEvent arg0)
            {
                defaultfontSize = Double.parseDouble(DeafaultField.getText());
            }

            private void printMyLines()
            {

            }
        });
        ConvertField.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent arg0)
            {
                toConvert = Double.parseDouble(ConvertField.getText());
            }
            public void insertUpdate(DocumentEvent arg0)
            {
                toConvert = Double.parseDouble(ConvertField.getText());
            }

            public void removeUpdate(DocumentEvent arg0)
            {

            }

            private void printMyLines()
            {

            }
        });
        ConvertField2.getDocument().addDocumentListener(new DocumentListener()
        {

            public void changedUpdate(DocumentEvent arg0)
            {
                Convert = Double.parseDouble( ConvertField2.getText());
            }
            public void insertUpdate(DocumentEvent arg0)
            {
                Convert= Double.parseDouble( ConvertField2.getText());
            }

            public void removeUpdate(DocumentEvent arg0)
            {

            }

            private void printMyLines()
            {

            }
        });

    }
    public void addPropertyChangeListener (
            PropertyChangeListener p) {
        changes.addPropertyChangeListener (p);
    }
    public void removePropertyChangeListener (
            PropertyChangeListener p) {
        changes.removePropertyChangeListener (p);
    }
    public void addVetoableChangeListener (VetoableChangeListener v) {
        vetoes.addVetoableChangeListener (v);
    }
    public void removeVetoableChangeListener (VetoableChangeListener v) {
        vetoes.removeVetoableChangeListener (v);
    }
    public void setNumbers (int numbers) throws PropertyVetoException {
        int oldNumber = new Integer (this.numbers);
        vetoes.fireVetoableChange ( "numbers", oldNumber, new Float ( numbers));
        this.numbers = numbers;
        changes.firePropertyChange ( "salary", oldNumber, new Float (this.numbers));
    }
    public String getTitle(){
        return title;
    }
    public void setTile(String title){
        String oldTitle = new String(this.title);
        this.title=title;
        changes.firePropertyChange (
                "title", oldTitle, new Float (this.title));
    }

    public Color getKolor() {
        return kolor;
    }
    public void setKolor(Color nowyKolor) {
        kolor = nowyKolor;
        changeColorButton.setBackground(kolor);

        repaint();
    }
    public void paint(Graphics g) {
        changeColorButton.setBackground(kolor);
        g.setColor(kolor);

    }
    public double PixelToPoint(double pixel){
        return pixel*0.75;
    }


    public double PixelToRem(double pixel){
        return pixel/this.defaultfontSize;
    }


    public double PixelToPercent(double pixel){
        return (pixel/defaultfontSize)*100;
    }


    public double PointToPixel(double point){
        return point/0.75;
    }


    public double PointToPercent(double point){
        return (point/0.75)/defaultfontSize*100;
    }

    public double PointToRem(double point){
        return (point/0.75)/defaultfontSize*100;
    }


    public double RemToPixel(double rem){
        return rem*this.defaultfontSize;
    }


    public double RemToPercent(double rem){
        return this.defaultfontSize*100;
    }


    public double RemToPoint(double rem){
        return rem*0.75*this.defaultfontSize;
    }

    public double PercentToPixel(double percent){
        return (percent*defaultfontSize)/100;
    }


    public double PercentToPoint(double percent){
        return ((percent*0.75*defaultfontSize)/100);
    }


    public double PercentToRem(double percent){
        return percent/100;
    }
    ActionListener color = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton btn = (JButton) e.getSource();

            if(btn==changeColorButton){
                if(getKolor()==white){
                    setKolor(black);
                }else{
                    setKolor(white);

                }

            }
        }
    };
        ActionListener listener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JRadioButton btn = (JRadioButton) e.getSource();

            if(btn==remRadioButton){
                wybor ="rem";
                System.out.println("aaa");
                pxRadioButton.setSelected(false);
                ptRadioButton.setSelected(false);
                radioButton1.setSelected(false);

            }else if(btn==remRadioButton1){

                pxRadioButton1.setSelected(false);
                ptRadioButton1.setSelected(false);
                radioButton2.setSelected(false);
                if(wybor==null){

                }else if( wybor=="rem"){

                    ConvertField2.setText(String.valueOf(toConvert));
                }else if( wybor=="px"){
                    ConvertField2.setText(String.valueOf(PixelToRem(toConvert)));
                }else if( wybor=="pt"){
                    ConvertField2.setText(String.valueOf(PointToRem(toConvert)));
                }else if( wybor=="percent"){
                    ConvertField2.setText(String.valueOf(PercentToRem(toConvert)));

                }
            }else if(btn==pxRadioButton){

                remRadioButton.setSelected(false);
                ptRadioButton.setSelected(false);
                radioButton1.setSelected(false);
                wybor="px";
            }else if(btn==pxRadioButton1){
                remRadioButton1.setSelected(false);
                ptRadioButton1.setSelected(false);
                radioButton2.setSelected(false);
                if(wybor==null){

                }else if( wybor=="rem"){
                    ConvertField2.setText(String.valueOf(toConvert));

                }else if( wybor=="px"){
                    ConvertField2.setText(String.valueOf(RemToPixel(toConvert)));

                }else if( wybor=="pt"){
                    ConvertField2.setText(String.valueOf(PointToPixel(toConvert)));

                }else if( wybor=="percent"){
                    ConvertField2.setText(String.valueOf(PercentToPixel(toConvert)));

                }
            }else if(btn==ptRadioButton){
                remRadioButton.setSelected(false);
                pxRadioButton.setSelected(false);
                radioButton1.setSelected(false);
                wybor="pt";
            }else if(btn==ptRadioButton1){
                remRadioButton1.setSelected(false);
                pxRadioButton1.setSelected(false);
                radioButton2.setSelected(false);
                if(wybor==null){

                }else if( wybor=="rem"){
                    ConvertField2.setText(String.valueOf(RemToPoint(toConvert)));

                }else if( wybor=="px"){
                    ConvertField2.setText(String.valueOf(PixelToPoint(toConvert)));
                }else if( wybor=="pt"){
                    ConvertField2.setText(String.valueOf(toConvert));

                }else if( wybor=="percent"){
                    ConvertField2.setText(String.valueOf(PercentToPoint(toConvert)));

                }
            }else if(btn==radioButton1){
                remRadioButton.setSelected(false);
                pxRadioButton.setSelected(false);
                ptRadioButton.setSelected(false);
                wybor="percent";
            }else if(btn==radioButton2){
                remRadioButton1.setSelected(false);
                pxRadioButton1.setSelected(false);
                ptRadioButton1.setSelected(false);
                if(wybor==null){
                    ConvertField2.setText(String.valueOf(toConvert));
                }else if( wybor=="rem"){
                    ConvertField2.setText(String.valueOf(RemToPercent(toConvert)));

                }else if( wybor=="px"){
                    ConvertField2.setText(String.valueOf(PixelToPercent(toConvert)));

                }else if( wybor=="pt"){
                    ConvertField2.setText(String.valueOf(PointToPercent(toConvert)));

                }else if( wybor=="percent"){
                    ConvertField2.setText(String.valueOf(toConvert));

                }
            }
        }
    };
    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
