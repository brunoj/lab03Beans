package bean;
import java.awt.*;
import java.awt.event.*;
import java.beans.*;
public class javabeanCustomizer extends Panel implements Customizer, KeyListener {
    private javabean target;
    private TextField titleField;
    private PropertyChangeSupport support = new PropertyChangeSupport(this);
    public void setObject(Object obj) {
        target = (javabean) obj;
        Label t1 = new Label("Salary :");
        add(t1);
        titleField = new TextField( String.valueOf(target.getTitle()), 20);
        add(titleField);
        titleField.addKeyListener(this);
    }
    public Dimension getPreferredSize() {
        return new Dimension(225,50);
    }
    public void keyPressed(KeyEvent e) {}
    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e) {
        Object source = e.getSource();
        if (source==titleField) {
            String txt = titleField.getText();
            try {
                target.setTile( (txt ));
            } catch (NumberFormatException ex) {
                titleField.setText( String.valueOf(target.getTitle()) );
            }
            support.firePropertyChange("", null, null);
        }
    }
    public void addPropertyChangeListener(PropertyChangeListener l) {
        support.addPropertyChangeListener(l);
    }
    public void removePropertyChangeListener(PropertyChangeListener l) {
        support.removePropertyChangeListener(l);
    }
}